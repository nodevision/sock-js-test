import React, {useRef} from 'react';
import SockJsClient from "react-stomp";
import './App.css';

function App() {
    const clientRef = useRef();

    return (
    <div className="App">
      <SockJsClient
          url="https://staging-2.relaypay.io/websocket"
          topics={[`/topic/1`,`/topic/echo`]}
          onMessage={async msg =>{
              console.log(msg);
          }}
            ref={client =>{
                clientRef.current = client;
            }}
      />
    </div>
  );
}

export default App;
